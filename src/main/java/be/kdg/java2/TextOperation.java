package be.kdg.java2;

@FunctionalInterface
public interface TextOperation {
    String operate(String text);
}
