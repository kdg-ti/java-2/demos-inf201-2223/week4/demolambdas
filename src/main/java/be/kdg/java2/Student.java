package be.kdg.java2;

public class Student {
    private int id;
    private String name;
    private double length;

    public Student(int id, String name, double length) {
        this.id = id;
        this.name = name;
        this.length = length;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLength() {
        return length;
    }

    public String sign(String text){
        return text + "\n Signed by " + name + ", id:" + id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", length=" + length +
                '}';
    }
}
