package be.kdg.java2;

import java.util.Comparator;

public class CompareStudentOpLength implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        if (o1.getLength()<o2.getLength()) return -1;
        if (o1.getLength()>o2.getLength()) return 1;
        return 0;
    }
}
