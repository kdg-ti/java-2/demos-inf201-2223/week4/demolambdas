package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class TextWorkerWithExistingInterface {
    private String text;
    private List<Function<String, String>> operations = new ArrayList<>();

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void addOperation(Function<String, String> operation){
        operations.add(operation);
    }

    public void execute(){
        for (Function<String, String> operation : operations) {
            text = operation.apply(text);
        }
    }
}
