package be.kdg.java2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student(1, "jos", 1.98));
        students.add(new Student(2, "an", 1.8));
        students.add(new Student(3, "jef", 2));
        students.add(new Student(4, "marie", 1.67));
        students.add(new Student(5, "frank", 1.43));
        students.add(new Student(6, "wtine", 1.54));

//        Collections.sort(students, new CompareStudentOpLength());
//        System.out.println(students);
//        Collections.sort(students, new Comparator<Student>() {
//            @Override
//            public int compare(Student o1, Student o2) {
//                if (o1.getLength()<o2.getLength()) return -1;
//                if (o1.getLength()>o2.getLength()) return 1;
//                return 0;
//            }
//        });

        Collections.sort(students, (o1, o2) -> {
            if (o1.getLength() < o2.getLength()) return -1;
            if (o1.getLength() > o2.getLength()) return 1;
            return 0;
        });
        System.out.println(students);

//        TextWorker textWorker = new TextWorker();
//        textWorker.addOperation(text -> text.toLowerCase());
//        textWorker.addOperation(text -> text.replace('a','e'));
//        textWorker.addOperation(text -> new StringBuilder(text).reverse().toString());
//        textWorker.setText("Nóg straffer dan de CL-finale van Courtois: de cijfers die aantonen dat Mignolet wondermatch keepte");
//        textWorker.execute();
//        System.out.println(textWorker.getText());
//        textWorker.setText("Bananen zijn gezond...");
//        textWorker.execute();
//        System.out.println(textWorker.getText());

        Student dirk = new Student(17,"dirk",1.78);
        TextWorkerWithExistingInterface textWorker = new TextWorkerWithExistingInterface();
        textWorker.addOperation(String::toLowerCase);
        textWorker.addOperation(text -> text.replace('a', 'e'));
        textWorker.addOperation(text -> new StringBuilder(text).reverse().toString());
        textWorker.addOperation(dirk::sign);
        textWorker.setText("Nóg straffer dan de CL-finale van Courtois: de cijfers die aantonen dat Mignolet wondermatch keepte");
        textWorker.execute();
        System.out.println(textWorker.getText());
        textWorker.setText("Bananen zijn gezond...");
        textWorker.execute();
        System.out.println(textWorker.getText());

        StudentUtilities su = new StudentUtilities();
        su.setStudents(students);
        Student student = su.findFirstMatch(s -> s.getLength() > 1.90);
        System.out.println(student);
    }
}
