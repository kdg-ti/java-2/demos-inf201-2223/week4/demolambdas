package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;

public class TextWorker {
    private String text;
    private List<TextOperation> operations = new ArrayList<>();

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void addOperation(TextOperation operation){
        operations.add(operation);
    }

    public void execute(){
        for (TextOperation operation : operations) {
            text = operation.operate(text);
        }
    }
}
