package be.kdg.java2;

import java.util.List;
import java.util.function.Predicate;

public class StudentUtilities {
    private List<Student> students;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Student findFirstMatch(Predicate<Student> theTest) {
        for (Student student : students) {
            if (theTest.test(student)) return student;
        }
        return null;
    }
}
