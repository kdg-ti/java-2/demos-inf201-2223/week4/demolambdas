Demo week 4 lambda's
--------------------
- Lambda's zijn een verkorte notatie voor anonymous inner classes. Zie bijvoorbeeld de implementatie van Comparator
- Om een lambda te kunnen gebruiken heb je een @FunctionalInterface nodig. We schreven er zelf een voor TextWorker
- TextWorker houdt een lijst van operaties bij die op een text losgelaten kunnen worden, ideale kandidaten om lambda's te zijn
- Bij het gebruik van TextWorker kunnen we de lambda notatie gebruiken: handig en overzichtelijk!
- We hoeven de interface niet zelf te schrijven, er zitten een hoop functional interfaces in java.util.function
- We kunnen een Function<String, String> gebruiken
- Ander voorbeeld van @FunctionalInterface: een Predicate. We gebruiken die in de StudentUtilities klasse
- Ten slotte tonen we nog het gebruik van method references aan: dirk::sign en String::toLowercase zijn voorbeelden
